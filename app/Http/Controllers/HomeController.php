<?php
/**
 * Created by PhpStorm.
 * User: nico95k
 * Date: 14/08/2019
 * Time: 19:24
 */

namespace App\Http\Controllers;
use App\GeoJson;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }

    public function save(Request $request){

        if($request->hasFile('geoJson')){
            $request->validate([
                'geoJson' => 'required|file|max:10000',
            ]);
            $data = json_decode(file_get_contents($request->file('geoJson')),true);

            $geoJson = new GeoJson($data);
            $geoJson->buildCoordinates();
            $coordinates = $geoJson->getCoordinates();

            return view('home',compact('coordinates'));
        }else{
            echo 'no file attached';
        }


    }



}
