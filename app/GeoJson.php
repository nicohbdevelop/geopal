<?php
/**
 * Created by PhpStorm.
 * User: nico95k
 * Date: 15/08/2019
 * Time: 14:02
 */

namespace App;


class GeoJson
{

    private $geoJson;
    private $coordinates = array();

    /**
     * GeoJson constructor.
     */
    public function __construct($json)
    {
        $this->geoJson = $json;
    }

    public function buildCoordinates(){

        for ($i=0;$i<sizeof($this->geoJson['features']);$i++){
           $coordinate = new Coordinate();
              for ($j=0;$j<=sizeof($this->geoJson['features'][$i]['geometry']['coordinates'])-1;$j++){
                  if ($j == 0){
                      $coordinate->setLongitude($this->geoJson['features'][$i]['geometry']['coordinates'][$j]);
                  }else{
                      $coordinate->setLatitude($this->geoJson['features'][$i]['geometry']['coordinates'][$j]);
                  }
              }
              array_push($this->coordinates,$coordinate);
        }
    }

    public function getCoordinates(){
        return $this->coordinates;
    }







}
