<?php
/**
 * Created by PhpStorm.
 * User: nico95k
 * Date: 15/08/2019
 * Time: 13:52
 */

namespace App;


class Coordinate
{

    private $latitude;
    private $longitude;


    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }



}
