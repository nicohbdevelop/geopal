@extends('base')

@section('css')
@endsection


@section('content')
    <div class="container-fluid">
        <div class="row mt-2">
            <div class="col-md-12">
                <h1 class="text-center">GeoPal Project</h1>
                <hr />
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row mb-4 mt-4">
            <div class="col-md-4 text-center">
                <div class="m-2 p-2 bg-light h-100">
                    <h4>Step 1</h4>
                    <hr />
                    <form action="{{url('upload')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <span>
                            <input type="file" name="geoJson" class="form-control mb-4">
                            <button class="w-100" type="submit">Upload</button>
                        </span>
                    </form>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="m-2 p-2 bg-light h-100">

                    <h4>Step 2</h4>
                    <hr />
                    <button class="w-100" type="button" data-toggle="modal" id="changeButton" data-target="#exampleModal" style="cursor: help">
                        Lasso
                    </button>

                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Selected Coordinates</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body row" id="selectedCoordinates">

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="m-2 p-2 bg-light h-100">
                    <h4>Step 3</h4>
                    <hr />
                    <select class="w-100" id="changeColor">
                        <option value="" disabled selected>Select your option</option>
                        <option value="http://maps.google.com/mapfiles/ms/icons/green-dot.png">Green</option>
                        <option value="http://maps.google.com/mapfiles/ms/icons/yellow-dot.png">Yellow</option>
                        <option value="http://maps.google.com/mapfiles/ms/icons/purple-dot.png">Purple</option>
                    </select>
                    <button class="w-100 mt-4" onclick="changeColor()" type="button">Change Color</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 h-100">
                <div id="map"></div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <!-- Scripts -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDMKSpPw_fgX7ifFZ5XPTofmDDBy6s9Q6U&callback=initMap&libraries=drawing,places"
            async defer></script>

    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: {lat: 43.068887774169625, lng: -7.55859375}
            });

            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.MARKER,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['rectangle']
                },
                markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
            });
            drawingManager.setMap(map);

            addOnRectangleListener(map ,drawingManager);
        }

        function addOnRectangleListener(map, drawingManager){
            google.maps.event.addListener(drawingManager, 'overlaycomplete', function (polygon) {
                var NE = polygon.overlay.getBounds().getNorthEast();
                var SW = polygon.overlay.getBounds().getSouthWest();

                @isset($coordinates)
                    var initialPosition = {lat: {!! $coordinates[0]->getLatitude() !!}, lng: {!! $coordinates[0]->getLongitude() !!}};
                    map.setCenter(initialPosition);

                    @foreach($coordinates as $coordinate)
                        var latitude = {!! $coordinate->getLatitude() !!};
                        var longitude = {!! $coordinate->getLongitude() !!};
                        if ((latitude < NE.lat()) && (latitude > SW.lat())){
                            if ((longitude < NE.lng()) && (longitude > SW.lng())){

                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: {lat: latitude, lng: longitude},
                                    title: "marker"
                                });

                                setMarkerInfo(marker);
                                addSelectedMarker(marker);
                            }
                        }
                    @endforeach
                @endisset
            });
        }

        var selectedMarkers = [];
        function addSelectedMarker(marker){
            selectedMarkers.push(marker);

            @isset($coordinates)
                document.getElementById('selectedCoordinates').innerHTML+=
                    '<div class="col-md-2"><span>Coordinate</span></div>'+
                    '<div class="col-md-5">Lat: {{$coordinate->getLatitude()}}</div>'+
                    '<div class="col-md-5">Lng: {{$coordinate->getLongitude()}}</div>';
            @endisset
        }

        function setMarkerInfo(marker){
            var contentString = '<p>'+"Latitude: "+marker.getPosition().lat()+'</p><p>Longitude: '+marker.getPosition().lng();

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });
        }

        function changeColor(){
            var color = document.getElementById('changeColor').value;

            for(var i=0;i<selectedMarkers.length;i++){
               selectedMarkers[i].setIcon(color);
            }
        }


    </script>



@endsection
